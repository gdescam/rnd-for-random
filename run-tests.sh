#!/usr/bin/env bash
#-----------------------------------------------------------
# Project : RND for RANDOM
# Script  : run-tests.sh
# Author  : Gregory Descamps
# Date    : 20/08/2024
# Depend. : -
#-----------------------------------------------------------

TEST_SCRIPTS=("./rnd-itest" "./rnd-stat-itest")

export DEBUG_MODE=0

for TEST_SCRIPT in "${TEST_SCRIPTS[@]}"; do
	"$TEST_SCRIPT" > /dev/null 2>&1
	STATUS=$?

	if [ $STATUS -ne 0 ]; then
		echo "$TEST_SCRIPT failed"
		exit 1
	else
		echo "$TEST_SCRIPT passed"
	fi
done

echo "All tests passed successfully."